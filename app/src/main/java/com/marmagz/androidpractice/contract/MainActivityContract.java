package com.marmagz.androidpractice.contract;

import com.marmagz.androidpractice.model.Heroes;

import java.util.List;

import retrofit2.Call;

public interface MainActivityContract {

    interface View{
        void initView();

        void setViewData(String data);
    }
    interface Model{
        String getData();
    }
    interface Presenter{
        void onClick(android.view.View view);
    }
}
