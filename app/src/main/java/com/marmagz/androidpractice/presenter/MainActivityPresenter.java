package com.marmagz.androidpractice.presenter;

import android.view.View;

import com.marmagz.androidpractice.contract.MainActivityContract;
import com.marmagz.androidpractice.model.MainActivityModel;
import com.marmagz.androidpractice.view.MainActivity;

public class MainActivityPresenter implements MainActivityContract.Presenter{
    private MainActivity mView;
    private MainActivityContract.Model mModel;

    public MainActivityPresenter(MainActivity view){
        mView = view;
        initPresenter();
    }
    private void initPresenter(){
        mModel = new MainActivityModel();
        mView.initView();
    }

    @Override
    public void onClick(View view) {
        String data = mModel.getData();
        mView.setViewData(data);
    }
}
