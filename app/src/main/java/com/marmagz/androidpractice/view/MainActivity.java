package com.marmagz.androidpractice.view;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.marmagz.androidpractice.contract.MainActivityContract;
import com.marmagz.androidpractice.model.Heroes;
import com.marmagz.androidpractice.R;
import com.marmagz.androidpractice.Rest;
import com.marmagz.androidpractice.presenter.MainActivityPresenter;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity implements MainActivityContract.View {
    private Button buttonCheck;
    private TextView textViewResponse;
    private MainActivityContract.Presenter mPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mPresenter = new MainActivityPresenter(this);

    }

//    private void getHeroes(){
//        Retrofit retrofit = new Retrofit.Builder()
//                .baseUrl(Rest.BASE_URL)
//                .addConverterFactory(GsonConverterFactory.create())
//                .build();
//
//        Rest restApi = retrofit.create(Rest.class);
//        Call<List<Heroes>> call = restApi.getHeroes();
//
//        call.enqueue(new Callback<List<Heroes>>() {
//            @Override
//            public void onResponse(Call<List<Heroes>> call, Response<List<Heroes>> response) {
//                List<Heroes> responseList = response.body();
//                String[] heroes = new String[responseList.size()];
////                for (int i = 0; i < responseList.size(); i++) {
////                    heroes[i] = responseList.get(i).getName();
////                }
//                Log.i("RESPONSE",new Gson().toJson(response));
//                textViewResponse.setText("Response: " + new Gson().toJson(response));
//            }
//
//            @Override
//            public void onFailure(Call<List<Heroes>> call, Throwable t) {
//                Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_SHORT).show();
//                Log.e("ERROR", t.getMessage());
//                textViewResponse.setText("Response: " + t.getMessage());
//            }
//        });
//    }

    @Override
    public void initView() {
        buttonCheck = findViewById(R.id.buttonCheck);
        textViewResponse = findViewById(R.id.textViewResponse);
        buttonCheck.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mPresenter.onClick(v);
            }
        });
    }

    @Override
    public void setViewData(String data) {
        textViewResponse.setText(data);
    }

}
