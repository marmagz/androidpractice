package com.marmagz.androidpractice.model;

import com.google.gson.Gson;
import com.marmagz.androidpractice.Rest;
import com.marmagz.androidpractice.contract.MainActivityContract;

import java.util.Arrays;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivityModel implements MainActivityContract.Model{


    @Override
    public String getData() {
        return "Hello World!";
    }
}
