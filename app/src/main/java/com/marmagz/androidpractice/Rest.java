package com.marmagz.androidpractice;

import com.marmagz.androidpractice.model.Heroes;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

public interface Rest {

    String BASE_URL = "http://10.0.2.2:8080/";

    @GET("testing")
    Call<List<Heroes>> getHeroes();

}
