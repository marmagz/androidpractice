package com.marmagz.androidpractice;

import com.github.tomakehurst.wiremock.WireMockServer;
import com.github.tomakehurst.wiremock.client.WireMock;

import org.junit.Before;
import org.junit.Test;

import static com.github.tomakehurst.wiremock.client.WireMock.aResponse;
import static com.github.tomakehurst.wiremock.client.WireMock.containing;
import static com.github.tomakehurst.wiremock.client.WireMock.equalTo;
import static com.github.tomakehurst.wiremock.client.WireMock.get;
import static com.github.tomakehurst.wiremock.client.WireMock.matching;
import static com.github.tomakehurst.wiremock.client.WireMock.notMatching;
import static com.github.tomakehurst.wiremock.client.WireMock.post;
import static com.github.tomakehurst.wiremock.client.WireMock.stubFor;
import static com.github.tomakehurst.wiremock.client.WireMock.unauthorized;
import static com.github.tomakehurst.wiremock.client.WireMock.urlEqualTo;
import static org.junit.Assert.*;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class ExampleUnitTest {
    @Test
    public void exactUrlOnly() {
        stubFor(post("/sorry-no")
                .willReturn(unauthorized()));
    }
    @Test
    public void headerMatching() {
        stubFor(post(urlEqualTo("/testingjson"))
                .withHeader("Content-Type", equalTo("application/json"))
                .withQueryParam("search_term", equalTo("WireMock"))
                .willReturn(aResponse()
                        .withStatus(200)));
    }
    @Test
    public void bodyMatching() {
        stubFor(post(urlEqualTo("/with/body"))
                .withRequestBody(matching("OK"))
                .withRequestBody(notMatching("ERROR"))
                .willReturn(aResponse().withStatus(200)));
    }
    @Test
    public void postTest(){
        stubFor(post(urlEqualTo("/baeldung/wiremock"))
                .withHeader("Content-Type", equalTo("application/json"))
                .withRequestBody(containing("\"testing-library\": \"WireMock\""))
                .withRequestBody(containing("\"creator\": \"Tom Akehurst\""))
                .withRequestBody(containing("\"website\": \"wiremock.org\""))
                .willReturn(aResponse()
                        .withStatus(200)));
    }
}